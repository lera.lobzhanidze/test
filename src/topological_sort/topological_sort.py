import numpy as np


def dfs(time_mark, vertex, matrix, visited):
    stack = []
    time_stack = []
    stack.append(vertex)
    time_stack.append([vertex, time_mark])
    time_mark_loc = time_mark + 1
    while len(stack) > 0:
        cur_vertex = stack.pop()
        if visited[cur_vertex] == 1:
            continue
        for j in range(matrix.shape[1]):
            if matrix[cur_vertex][j] == 1 and visited[j] != 1:
                stack.append(j)
                time_stack.append([j, time_mark_loc])
                time_mark_loc = time_mark_loc + 1
                visited[cur_vertex] = 1
        visited[cur_vertex] = 1
    return [time_stack, time_mark_loc]


def run(matrix):
    time_mark_acc = 0
    order = []
    visited = np.zeros(matrix.shape[0])

    for i in range(matrix.shape[0]):
        time_stack, time_mark = dfs(time_mark_acc, i, matrix, visited)
        time_mark_acc = time_mark
        order.append(time_stack)
    return list(reversed(order))
