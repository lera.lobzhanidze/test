import unittest
import numpy as np
import src.topological_sort.topological_sort as source


class TopologicalSortTestCase(unittest.TestCase):
    def test_case_1(self):
        matrix = np.array([[0, 0, 0, 0],
                           [1, 0, 0, 0],
                           [1, 0, 0, 0],
                           [0, 1, 1, 0]])
        self.assertEqual(source.run(matrix), [[[3, 3]], [[2, 2]], [[1, 1]], [[0, 0]]])


if __name__ == '__main__':
    unittest.main()
